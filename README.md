Command line instructions

Git global setup
git config --global user.name "USERNAME"
git config --global user.email "EMAIL ADDRESS"

Create a new repository

mkdir 3DVAM
cd 3DVAM
git init
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin git@gitlab.com:USERNAME/3DVAM.git
git push -u origin master

Push an existing Git repository

cd existing_git_repo
git remote add origin git@gitlab.com:USERNAME/3DVAM.git
git push -u origin master